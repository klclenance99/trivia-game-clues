from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

from postgres.routers import (
    categories as psql_categories,
    clues as psql_clues,
    games as psql_games,
    custom_games as psql_custom_games,
)

app = FastAPI()


origins = [
    "http://localhost:8000",
    os.environ.get("CORS_HOST", None),
    "http://localhost:3000",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(psql_categories.router)
app.include_router(psql_clues.router)
app.include_router(psql_games.router)
app.include_router(psql_custom_games.router)

