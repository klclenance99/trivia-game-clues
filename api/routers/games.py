from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg



router = APIRouter()

class Message(BaseModel):
    message: str

@router.get(
    "/api/games/{game_id}",
    # response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_game(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT games.id, games.episode_id, games.aired, games.canon, SUM(clues.value)
                FROM games
                LEFT OUTER JOIN clues
                  ON games.id = clues.game_id
                WHERE games.id = %s
                GROUP BY games.id
            """
            ,[game_id]
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            record = {}
            print(cur.description)
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]

            return record

@router.post(
    "/api/custom-games/",
    # response_model=ClueOut,
    responses={404: {"model": Message}},
)
def create_custom_game(response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            with conn.transaction():
                cur.execute(
                        """
                        INSERT INTO game_definitions(created_on)
                        VALUES (CURRENT_TIMESTAMP)
                        RETURNING id;
                    """
                    )
                game_id = cur.fetchone()[0]
                cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, categories.title AS cxtitle, categories.canon AS cxcanon, categories.id AS cxid 
                FROM clues
                JOIN categories
                  ON clues.category_id = categories.id
                ORDER BY RANDOM() LIMIT 30
            """,
            )
                for clue_row in cur.description():
                        cursor.execute(
                            """
                            INSERT INTO game_definition_clues (game_definition_id, clue_id)
                            VALUES (%s, %s);
                        """,
                            [new_game_id, clue_row[0]],
                        )

            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            record = {}
            print(cur.description)
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]

            return record