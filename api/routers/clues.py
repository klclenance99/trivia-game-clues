from .categories import CategoryWithClueCount
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
import random

router = APIRouter()


class Message(BaseModel):
    message: str

class ClueOut(BaseModel):
  id: int
  answer: str
  question: str
  value: int
  invalid_count: int
  category: CategoryWithClueCount
  canon: bool


class Clues(BaseModel):
    page_count: int
    clues: list[ClueOut]


@router.get(
    "/api/clues/{clue_id}",
    # response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, categories.title AS cxtitle, categories.canon AS cxcanon, categories.id AS cxid 
                FROM clues
                JOIN categories
                  ON clues.category_id = categories.id
                WHERE clues.id = %s
            """
            ,[clue_id]
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            record = {}
            print(cur.description)
            for i, column in enumerate(cur.description):
                if 'cx' in column.name and record.get('category') is None:
                    record['category'] = {}
                    record['category'][column.name[2:]] = row[i]
                    continue
                elif 'cx' in column.name:
                    record['category'][column.name[2:]] = row[i]
                    continue
                record[column.name] = row[i]

            return record

@router.get(
    "/api/random_clue",
    # response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_random_clue(response: Response, limit):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, categories.title AS cxtitle, categories.canon AS cxcanon, categories.id AS cxid 
                FROM clues
                JOIN categories
                  ON clues.category_id = categories.id
                ORDER BY RANDOM() LIMIT 1
            """,
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            record = {}
            print(cur.description)
            for i, column in enumerate(cur.description):
                if 'cx' in column.name and record.get('category') is None:
                    record['category'] = {}
                    record['category'][column.name[2:]] = row[i]
                    continue
                elif 'cx' in column.name:
                    record['category'][column.name[2:]] = row[i]
                    continue
                record[column.name] = row[i]

            return record

@router.get("/api/clues")
def clues_list(page: int = 0):
    # Uses the environment variables to connect
    # In development, see the docker-compose.yml file for
    #   the PG settings in the "environment" section
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, categories.title AS cxtitle, categories.canon AS cxcanon, categories.id AS cxid 
                FROM clues
                JOIN categories
                  ON clues.category_id = categories.id
                LIMIT 100 OFFSET %s
            """,
                [page * 100],
            )

            results = {"clues": []}
            for row in cur.fetchall():
                record = {}
                for i, column in enumerate(cur.description):
                    if 'cx' in column.name and record.get('category') is None:
                        record['category'] = {}
                        record['category'][column.name[2:]] = row[i]
                        continue
                    elif 'cx' in column.name:
                        record['category'][column.name[2:]] = row[i]
                        continue
                    record[column.name] = row[i]
                results['clues'].append(record)

            cur.execute(
                """
                SELECT COUNT(*) FROM categories;
            """
            )
            raw_count = cur.fetchone()[0]
            page_count = (raw_count // 100) + 1

            return results

@router.delete(
    "/api/clues/{clue_id}",
    #response_model=Message,
    responses={400: {"model": Message}},
)
def remove_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE id = %s;
                """,
                [clue_id],
                )
            cur.execute(
                f"""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, clues.canon, categories.title AS cxtitle, categories.canon AS cxcanon, categories.id AS cxid 
                FROM clues
                JOIN categories
                  ON clues.category_id = categories.id
                WHERE clues.id = %s
            """
            ,[clue_id]
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            record = {}
            # print(cur.description)
            for i, column in enumerate(cur.description):
                if 'cx' in column.name and record.get('category') is None:
                    record['category'] = {}
                    record['category'][column.name[2:]] = row[i]
                    continue
                elif 'cx' in column.name:
                    record['category'][column.name[2:]] = row[i]
                    continue
                record[column.name] = row[i]
            return record