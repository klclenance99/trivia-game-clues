- I completed the backend of this project as part of the course I completed with Hack Reactor. Upon graduation I decided to refactor the backend of the project and add a frontend to the project so that the trivia game would be playable. 

Programming Languages Used:
   - Python 
   - Javascript
   - SQL

Technologies Used:
  - psycopg
  - fastapi
  - react.JS
  - postgresSQL
