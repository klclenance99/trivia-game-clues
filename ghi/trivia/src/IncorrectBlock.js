import React, { useEffect } from 'react';
function IncorrectBlock(props) {
    const setint = props.setint
    const int = props.int
    useEffect(() => {setint(int + 1); }, [])
    return (
      <>
        <h5>Answer</h5>
        <p dangerouslySetInnerHTML={{ __html: props.answer}} />
      </>
    );
  }
  
  export default IncorrectBlock;