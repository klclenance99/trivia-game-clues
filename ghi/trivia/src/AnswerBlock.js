import CorrectBlock from "./CorrectBlock";
import IncorrectBlock from "./IncorrectBlock";
function AnswerBlock(props) {
    return (
      <>
        { props.winner
          ? <CorrectBlock answer={props.answer} score={props.score} setScore={props.setScore} int={props.int} setint={props.setint} clue={props.clue}/>
          : <IncorrectBlock answer={props.answer} score={props.score} setScore={props.setScore} int={props.int} setint={props.setint} clue={props.clue}/>
        }
      </>
    );
  }
  
  export default AnswerBlock;