import React, { useEffect, useState } from 'react';
import AnswerBlock from './AnswerBlock';
import GuessBlock from './GuessBlock';

function ClueBody(props) {
  const [showAnswer, setShowAnswer] = useState(false);
  const [text, setText] = useState('');
  const setWinner = props.setWinner

  useEffect(() => {
    setWinner(text.toLowerCase() === props.clue.answer.toLowerCase())
  }, [showAnswer])

  useEffect(() => {
    setShowAnswer(false);
    setText('');
  }, [props.clue]);

  return (
    <div className="card-body">
      <div className="clue-card card-text d-flex flex-column justify-content-center">
        <h5>{props.clue.category.title}</h5>
        <p dangerouslySetInnerHTML={{ __html: props.clue.question}} />
        { showAnswer
          ? <AnswerBlock answer={props.clue.answer} winner={props.winner} setScore={props.setScore} score={props.score} int={props.int} setint={props.setint} clue={props.clue}/>
          : <GuessBlock text={text} setText={setText} setShowAnswer={setShowAnswer} />
        }
      </div>
    </div>
  )
};

export default ClueBody;
