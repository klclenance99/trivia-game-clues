import LoadingBody from './LoadingBody';
import ClueBody from './ClueBody';
import ErrorBody from './ErrorBody';
import React, { useEffect, useState } from 'react';
function ClueCard() {
    const [clue, setclue] = useState(false)
    const [error, seterror] = useState(false)
    const [int, setint] = useState(1)
    const [score, setScore] = useState(0)
    const [winner, setWinner] = useState(false);
    if (!localStorage.getItem("highscore")) {
      localStorage.setItem("highscore", "0")
    }

    useEffect(() => {loadClue()}, []);

    async function loadClue() {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/postgres/clues/random-clue`);
      if (response.ok) {
        const cluu = await response.json();
        cluu.question = cluu.question.replace('\\\\', '');
        cluu.answer = cluu.answer.replace('\\\\', '');
        setclue(cluu)
        seterror(false)
      } else {
        seterror(true)
      }
  }
  let nextvar = "btn btn-success"
  let replay = "btn btn-success d-none"
  if (int > 7) {
    nextvar = replay
    replay = "btn btn-success"
    if (parseInt(localStorage.getItem("highscore")) < score) {
      localStorage.setItem("highscore", score.toString())
      alert("Congratulations! New High Score!!")
    }
    else {
    alert("Game Over! Click below to play again")
    }

  }
    return (
        <div>
        <div className="col-6 offset-3">
        </div>
        <p className="mt-4">
          <button className={nextvar} onClick={() => loadClue()}>Next clue</button>
          <button className={replay}><a href="/">Play Again</a></button>
        </p>
        <p>score: {score}             question {int} of 7</p>
        <div className="card col-6 offset-3">
          { error
            ? <ErrorBody />
            : clue
            ? <ClueBody clue={clue} setScore={setScore} score={score} setint={setint} int={int} winner={winner} setWinner={setWinner}/>
            : <LoadingBody />
          }
        </div>
        </div>
    );
  };
export default ClueCard;
