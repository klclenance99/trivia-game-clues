import React, { useEffect } from 'react';
function CorrectBlock(props) {
    const setint = props.setint
    const int = props.int
    const setScore = props.setScore
    const clueCopy = {...props.clue}
    var scoreCopy = props.score
    console.log(scoreCopy + clueCopy.value)
    useEffect(() => {setint(int); }, [])
    useEffect(() => {setScore(scoreCopy + clueCopy.value)}, [])
    return (
      <>
        <h5>Answer</h5>
        <p dangerouslySetInnerHTML={{ __html: props.answer}} />
        <div className="alert alert-success">WINNER</div>
      </>
    );
  }
  
  export default CorrectBlock;